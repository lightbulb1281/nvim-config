return {
    { 
        "folke/tokyonight.nvim",
        opts = {
            style = "night",
        }
    },

    {
        "ellisonleao/gruvbox.nvim",
    },

    {
      'patstockwell/vim-monokai-tasty'
    },

    { "catppuccin/nvim", as = "catppuccin" },

    { "rose-pine/neovim", name = "rose-pine" },
  
    -- Configure LazyVim to load gruvbox
    {
      "LazyVim/LazyVim",
      opts = {
        colorscheme = "tokyonight",
      },
    }
  }